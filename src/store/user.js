import { observable, computed, action } from 'mobx'
import { RestAPI } from '../utils/utils.js';
import Taro from '@tarojs/taro';

class userStore {
    @observable userInfo = {}
    
    //判断是否登录了拿到用户信息
    judgeLogin = () =>{
      var info = wx.getStorageSync("USERINFO");
      if(info!=null&&info!=''){
        this.userInfo = JSON.parse(info);
      }
    }
    
    //获取用户信息
    getUserInfo = (e) => {
      wx.showToast({
        title: '登录中...',
        icon: 'loading',
        duration: 60 * 1000
      })
      var info = e.detail.userInfo
      var _this = this;
      wx.login({
        success:res=>{
          console.log('res',res);
          info.code = res.code;
          var param = {
            data: {
              appid: 'wxa60cbf5ce8699683',
              secret: '0a6ba763f51806fc955678fc07a3d958',
              js_code: res.code
            }
          }
          console.log(param);
          RestAPI.invoke('/applet/appletGetUserInfo', param, function (res) {
            if(res.code == 200){
              console.log("---",res);
              var uInfo = res.data
              info.openid = uInfo.openid;
              if (uInfo.token != null && uInfo.token!=''){
                info.phone = uInfo.mobile;
                info.mobileCode = uInfo.mobileCode;
                info.token = uInfo.token;
                info.userId = uInfo.userId;
                info.inviteCode = uInfo.inviteCode
              }
              _this.userInfo = info;
              console.log(info)
              wx.setStorageSync("USERINFO", JSON.stringify(_this.userInfo));
              wx.hideToast();
            }else{
              wx.showToast({
                title: res.message,
                icon: 'none',
                duration: 3000
              })
            }
          }, function (err) {
            console.log(err);
          })
        }
      })
    }
}
export default new userStore()