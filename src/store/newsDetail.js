import { observable, computed, action } from 'mobx'
import { RestAPI } from '../utils/utils.js';
import Taro from '@tarojs/taro';
import { AtToast } from "taro-ui"

class newsDetailStore {
  @observable newsId = 32350;
  @observable newsInfo = {content:'',readCountStr:'',time:'',source:'',title:''};
  @observable commPage = 1;//评论页码
  @observable commList = [];//评论列表数据
  @observable commOver = false;//评论数据是否全部加载完
  @observable commType = 1;//1.评论 2.回复
  @observable source = 1;//1.资讯，2.（爆料）
  @observable content = '';//评论内容
  @observable commentId = null;//回复评论对应的ID，回复评论时需要
  @observable replyUser = {};//回复评论用户信息
  @observable replyIuputValue = '';//回复评论内容
  @observable commentState = false; // 评论状态
  @observable type = 1;//1表示资讯详情，2表示爆料详情
  @observable isCollect = false;//是否收藏次信息
  @observable commentNum = 0;//评论条数



  getNewsDetailByID = () => {
    wx.showToast({
      title: '数据加载中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var _this = this;
    var param = { data: { newsId: this.newsId } }
    RestAPI.invoke('/news/getNews', param, function (res) {
      wx.hideToast();
      if (res.code == 200) {
        var date = new Date(res.data.createdAt * 1000);
        res.data.time = date.toTimeString().substr(0, 5)
        if (res.data.imgUrl.indexOf("|") > 0) {
          var imgs = res.data.imgUrl.split("|");
          res.data.coverImg = imgs[0];
        } else {
          res.data.coverImg = res.data.imgUrl;
        }
        _this.newsInfo = res.data;
        var date = new Date(_this.newsInfo.createdAt * 1000);
        _this.newsInfo.time = date.toTimeString().substr(0, 5)
        var weekday = RestAPI.getWeekday((new Date(_this.newsInfo.createdAt * 1000)).getDay());
        _this.newsInfo.weekday = weekday
      } else {
        console.log(res.message);
      }
    }, function (err) {
      console.log(err);
    })
  }

//爆料详情
  getTipsterDetail = () =>{
    wx.showToast({
      title: '数据加载中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var _this = this;
    var param = { data: { newsId: this.newsId } }
    RestAPI.invoke('/applet/getTopicDetails', param, function (res) {
      wx.hideToast();
     // console.log(res.data);
      if (res.code == 200) {
        _this.newsInfo = res.data
      } else {
        console.log(res.message);
      }
    }, function (err) {
      console.log(err);
    })
  }

  //获取评论列表数据
  getNewsCommentList = () => {
    var _this = this;
    var param = {};
    if(this.type == 2){
      param = { data: { newsId: this.newsId, page: this.commPage, limit: 20, source:2 } }
    }else param = { data: { newsId: this.newsId, page: this.commPage, limit: 20 } }
    RestAPI.invoke('/news/getNewsCommentList', param, function (res) {
      console.log('.........',res)
      if (res.code == 200) {
        for (var v of res.data) {
          _this.commList.splice(_this.commList.length, 0, v);
        }
        if (res.data == null || res.data.length < 20) {
          _this.commOver = false;
        } else {
          _this.commPage++;
        }
      } else {
        console.log(res.message);
      }
    }, function (err) {
      console.log(err);
    })
  }

  @computed get formattedCommList() {
    return this.commList.slice();
  }

  //获取回复用户信息
  getReplyUser = (v) => {
    //console.log('user',v)
    if (this.replyUser.id != v.id){
       this.replyUser = v
       //清空评论内容
       this.replyIuputValue = ''
      
    }

    if (this.commentState == false) {
      this.commentState = true
    }
    
    
  }

  //获取回复内容
  getReplyInputValue = (v) => {
    this.replyIuputValue = v
    this.commentState = true
    
  }

  //评论资讯
  commentNews = () =>{
    var _this = this;
    wx.showToast({
      title: '评论发布...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var param = { 
      data: { 
        newsId: this.newsId,
        type:1,
        source:1,
        content: this.replyIuputValue,
      } 
    }
    console.log('-----------', param);
    RestAPI.execute('/news/submitComment', param, function (res) {
      if (res.code == 200) {
        _this.replyIuputValue = '';
        wx.showToast({
          title: '评论成功,' + res.message,
          icon: 'success',
          duration: 2000
        })
        _this.commList = [];
        _this.commPage = 1;
        _this.getNewsCommentList();
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none',
          duration: 3000
        })
      }
    }, function (err) {
      console.log(err);
    })
  }

  //回复评论
  replyNews = () => {
    var _this = this;
    wx.showToast({
      title: '回复发表中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var param = {
      data: {
        newsId: this.newsId,
        type: 1,
        source: 1,
        content: this.replyIuputValue,
        commentId: this.replyUser.id
      }
    }
    RestAPI.execute('/news/submitComment', param, function (res) {
      console.log(res);
      if (res.code == 200) {
        _this.commList = [];
        _this.commPage = 1;
        _this.getNewsCommentList();
        wx.showToast({
          title: '回复成功',
          icon: 'success',
          duration: 2000
        })
        _this.replyIuputValue = '';
        _this.replyUser = {};
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none',
          duration: 3000
        })
      }
    }, function (err) {
      console.log(err);
    })
  }

  //收藏此内容
  toCollect = () =>{
    var info = wx.getStorageSync("USERINFO");
    if (info == null || info == '') {
      wx.showToast({
        title: '账号未登录，无法查看收藏内容!',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    info = JSON.parse(info);
    if (info.token == null || info.token == '') {
      wx.showToast({
        title: '未关联账号，需先手机关联！',
        icon: 'none',
        duration: 3000
      });
      Taro.navigateTo({
        url: `/pages/user/bind-phone/index`
      })
      return;
    }
    wx.showToast({
      title: '收藏中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var _this = this;
    var param = { data: { newsId: this.newsId } }
    RestAPI.execute('/user/collection', param, function (res) {
      wx.hideToast();
      if (res.code == 200) {
        _this.isCollect = true;
      } else {
        wx.showToast({
          title: JSON.parse(res).message,
          icon: 'none',
          duration: 3000
        })
      }
    }, function (err) {
      console.log(err);
    })
  }

}
export default new newsDetailStore()