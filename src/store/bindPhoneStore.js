import { observable, computed, action } from 'mobx'
import { RestAPI } from '../utils/utils.js';
import Taro from '@tarojs/taro';

class bindPhoneStore {
  @observable phone = '';//手机号码
  @observable verCode = '';//验证码
  @observable fromCollect=false;//上个页面来源是否是收藏页面

//获取验证码
  getVerCode = () =>{
    var _this = this;
    var param = { data: { type: 1, mobile:this.phone}};
    RestAPI.invoke('/applet/appletSendMessage', param, function (res) {
      if (res.code == 200) {
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none',
          duration: 3000
        })
        console.log(res.message);
      }
    }, function (err) {
      console.log(err);
    })
  }

//注册
  registerEvent = (user_info) =>{
    wx.showToast({
      title: '验证数据中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var _this = this;
    var userInfo = JSON.parse(wx.getStorageSync("USERINFO"));
    var param = { 
      data: { 
        type: 1, 
        openId: userInfo.openid, 
        userIcon: userInfo.avatarUrl,
        nikeName: userInfo.nickName,
        mobile:this.phone,
        verifyCode:this.verCode
      } 
    };
    RestAPI.invoke('/applet/weiXinAppletLogin', param, function (res) {
      console.log("phone-----",res);
      wx.hideToast();
      if (res.code == 200) {
        var uInfo = res.data; 
        userInfo.phone = uInfo.mobile;
        userInfo.mobileCode = uInfo.mobileCode;
        userInfo.token = uInfo.token;
        userInfo.userId = uInfo.userId;
        userInfo.inviteCode = uInfo.inviteCode
        wx.setStorageSync("USERINFO", JSON.stringify(userInfo));
        wx.setStorageSync("HXUSERINFO", JSON.stringify(uInfo));
        user_info = userInfo;
        if (_this.fromCollect){
          Taro.navigateBack({ delta: 2 })
        }else
          wx.navigateBack({})
      } else {
        wx.showToast({
          title: JSON.parse(res).message,
          icon: 'none',
          duration: 3000
        })
        console.log(res.message);
      }
    }, function (err) {
      console.log(err);
    })
  }
}
export default new bindPhoneStore()