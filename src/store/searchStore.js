import { observable, computed, action } from 'mobx'
import { RestAPI } from '../utils/utils.js';
import Taro from '@tarojs/taro';

class searchStore {
  @observable counter = 0
  @observable newsPage = 1//推荐新闻加载页面
  @observable pageNum = 1//快讯新闻加载页面
  @observable tipsterPage = 1//爆料加载页面
  @observable isOpened = true//加载接口轻提示开关
  @observable bannerList = []//轮播图数据
  @observable news = []//新闻数据
  @observable newsList = []//新闻数据
  @observable loadOver = false//快讯数据是否加载完成
  @observable newsOver = false//推荐数据是否加载完成
  @observable fastNews = []//快讯数据
  @observable fastNewsList = []//快讯分组数据
  @observable tipster = []//爆料数据
  @observable tipsterList = []//爆料数据
  @observable tipsterOver = false//爆料数据是否加载完成
  @observable searchContent = '司法'//查找内容
  @observable searchPage = 1;
  @observable searchDataList = [];
  @observable searchOver = false;

  //推荐新闻数据
  // getNewListData = () => {
  //   var _this = this;
  //   var param = { data: { categoryId: 1, page: this.newsPage, limit: 20, key:this.searchContent } }
  //   RestAPI.invoke('/news/getNewsList', param, function (res) {
  //     if (res.code == 200) {
  //       if(res.data.length>0){
  //         _this.newsList = [];
  //       }else{
  //         wx.showToast({
  //           title: '查无包含此内容的推荐信息!',
  //           icon: 'none',
  //           duration: 2000
  //         });
  //       }
  //       for (var v of res.data) {
  //         var date = new Date(v.createdAt * 1000);
  //         v.time = date.toTimeString().substr(0, 5)
  //         _this.news.splice(_this.news.length, 0, v);
  //       }
  //       _this.newsList = _this.news;
  //       if (res.data == null || res.data.length < 20) {
  //         _this.newsOver = true;
  //       } else {
  //         _this.newsPage++;
  //       }
  //       console.log(res.data);
  //     } else {
  //       console.log(res.message)
  //     }
  //   }, function (err) {
  //     console.log(err);
  //   })
  // }

  // @computed get formattedSeachNewsList() {
  //   return this.newsList.slice();
  // }

  // //快讯新闻数据
  // getFastNewListData = () => {
  //   this.isOpened = true;
  //   this.loadOver = true;
  //   var _this = this;
  //   var param = { data: { categoryId: 2, page: this.newsPage, limit: 20, key: this.searchContent } }
  //   RestAPI.invoke('/news/getNewsList', param, function (res) {
  //     _this.isOpened = false;
  //     if (res.data.length > 0) {
  //       _this.fastNewsList = [];
  //     } else {
  //       wx.showToast({
  //         title: '查无包含此内容的快讯信息!',
  //         icon: 'none',
  //         duration: 2000
  //       });
  //     }
  //     var newsData = res.data;
  //     for (var v of newsData) {
  //       var date = new Date(v.createdAt * 1000);
  //       v.date = date.toLocaleDateString().replace(/\//g, "-");
  //       v.time = date.toTimeString().substr(0, 8)
  //       var weekday = RestAPI.getWeekday((new Date(v.date)).getDay());
  //       v.date = v.date + " " + weekday
  //       _this.fastNews.splice(_this.fastNews.length, 0, v)
  //     }
  //     var map = {}, dest = [];
  //     for (var i = 0; i < _this.fastNews.length; i++) {
  //       var ai = _this.fastNews[i];
  //       if (!map[ai.date]) { //依赖分组字段可自行更改！
  //         dest.push({
  //           date: ai.date, //依赖分组字段可自行更改！
  //           data: [ai]
  //         });
  //         map[ai.date] = ai; //依赖分组字段可自行更改！
  //       } else {
  //         for (var j = 0; j < dest.length; j++) {
  //           var dj = dest[j];
  //           if (dj.date == ai.date) { //依赖分组字段可自行更改！
  //             dj.data.push(ai);
  //             break;
  //           }
  //         }
  //       }
  //     }
  //     _this.fastNewsList = dest;
  //     if (newsData == null || newsData.length < 20) {
  //       _this.loadOver = true;
  //     } else {
  //       _this.pageNum++;
  //     }
  //   }, function (err) {
  //     console.log(err);
  //   })
  // }

  // @computed get formattedSeachFastList() {
  //   return this.fastNewsList.slice();
  // }

  // //爆料数据
  // getTipsterListData = () => {
  //   var _this = this;
  //   var param = { data: { categoryId: 3, page: this.newsPage, limit: 20, key: this.searchContent } }
  //   RestAPI.invoke('/news/getNewsList', param, function (res) {
  //     if (res.code == 200) {
  //       if (res.data.length > 0) {
  //         _this.tipsterList = [];
  //       } else {
  //         wx.showToast({
  //           title: '查无包含此内容的爆料信息!',
  //           icon: 'none',
  //           duration: 2000
  //         });
  //       }
  //       for (var v of res.data) {
  //         var date = new Date(v.createdAt * 1000);
  //         v.time = date.toTimeString().substr(0, 5)
  //         _this.tipster.splice(_this.tipster.length, 0, v);
  //       }
  //       _this.tipsterList = _this.tipster;
  //       if (res.data == null || res.data.length < 20) {
  //         _this.tipsterOver = true;
  //       } else {
  //         _this.tipsterPage++;
  //       }
  //     } else {
  //       console.log(res.message)
  //     }
  //   }, function (err) {
  //     console.log(err);
  //   })
  // }

  // @computed get formattedSeachTipsterList() {
  //   return this.tipsterList.slice();
  // }

  seachNewsData = () =>{
    var _this = this;
    var param = {
      data: {
        key: this.searchContent,
        type:0,
        page:this.searchPage,
        limit:20,
        currency:'CNY'
      } 
    }
    RestAPI.invoke('/news/globalSearch', param, function (res) {
      if (res.code == 200) {
        if (res.data.length > 0) {
          _this.searchDataList = [];
        } else {
          wx.showToast({
            title: '查无包含此内容的爆料信息!',
            icon: 'none',
            duration: 2000
          });
        }
        for (var v of res.data) {
          var date = new Date(v.createdAt * 1000);
          v.time = date.toTimeString().substr(0, 5)
          _this.searchDataList.splice(_this.news.length, 0, v);
        }   
        if (res.data == null || res.data.length < 20) {
          wx.showToast({
            title: '全部数据加载完毕',
            icon: 'none',
            duration: 3000
          })
          _this.searchOver = true;
        } else {
          _this.searchPage++;
        }
      } else {
        console.log(res.message)
      }
    }, function (err) {
      console.log(err);
    })
  }

  @computed get formattedSeachDataList() {
    return this.searchDataList.slice();
  }

}
export default new searchStore()