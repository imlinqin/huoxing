import { observable, computed, action } from 'mobx'
import { RestAPI } from '../utils/utils.js';
import Taro from '@tarojs/taro';

class homeStore {
  @observable counter = 0
  @observable newsPage = 1//推荐新闻加载页面
  @observable pageNum = 1//快讯新闻加载页面
  @observable tipsterPage = 1//爆料加载页面
  @observable isOpened = true//加载接口轻提示开关
  @observable bannerList = []//轮播图数据
  @observable news = []//新闻数据
  @observable newsList = []//新闻数据
  @observable loadOver = false//快讯数据是否加载完成
  @observable newsOver = false//推荐数据是否加载完成
  @observable fastNews = []//快讯数据
  @observable fastNewsList = []//快讯分组数据
  @observable tipster = []//爆料数据
  @observable tipsterList = []//爆料数据
  @observable tipsterOver = false//爆料数据是否加载完成
  @observable type = 1;
  @observable collectPage = 1//收藏页面数
  @observable collectList = []//收藏数据
  @observable collectOver = false//收藏数据是否加载完成

  //头部广告栏数据
  getBannerList = () => {
    var _this = this;
    var param = { data: { categoryId: 1 } }
    RestAPI.invoke('/news/getBannerList', param, function (res) {
      for(var v of res.data){
        var index = v.linkUrl.lastIndexOf("\/");
        v.newsID = v.linkUrl.substring(index + 1, v.linkUrl.length);
      }
      _this.bannerList = res.data;
    }, function (err) {
      console.log(err);
    })
  }


  @computed get formattedBannerList() {
    return this.bannerList.slice();
  }

  //推荐新闻数据
  getNewListData = () =>{
    var _this = this;
    var param = { data: { categoryId: 1, page:this.newsPage,limit:20}}
    RestAPI.invoke('/news/getNewsList', param, function (res) {
      wx.hideToast();
      if(res.code == 200){
        for(var v of res.data){
          var date = new Date(v.createdAt * 1000);
          v.time = date.toTimeString().substr(0, 5)
          _this.news.splice(_this.news.length,0,v);
        }
        _this.newsList = _this.news;
        if (res.data == null || res.data.length < 20) {
          _this.newsOver = false;
        } else {
          _this.newsPage++;
        }
      }else{
        console.log(res.message)
      }
    }, function (err) {
      console.log(err);
    })
  }

  @computed get formattedNewsList() {
    return this.newsList.slice();
  }
  
  //快讯新闻数据
  getFastNewListData = () => {
    var param = { data: { page: this.pageNum, limit: 20 } }
    this.isOpened = true;
    this.loadOver = true;
    var _this = this;
    var u_info = wx.getStorageSync("USERINFO");
    var openID = null;
    if (u_info != null && u_info != '') {
      openID = JSON.parse(u_info).openid;
    }
    RestAPI.invoke('/news/getNewsFlashList', param, function (res) {
      _this.isOpened = false;
      var newsData = res.data;
      wx.hideToast();
      for (var v of newsData){
        var date = new Date(v.createdAt * 1000);
        var Y = date.getFullYear();
        //月
        var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
        //日
        var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        //时
        var h = date.getHours();
        //分
        var m = date.getMinutes();
        //秒
        var s = date.getSeconds();
        v.day = date.toLocaleDateString().replace(/\//g, "-");
        v.time = date.toTimeString().substr(0, 8);
        if (RestAPI.timeFn(date.toLocaleDateString())==0){
          v.dayDiff = '今天';
        } else if (RestAPI.timeFn(date.toLocaleDateString()) == 1) {
          v.dayDiff = '昨天';
        } else if (RestAPI.timeFn(date.toLocaleDateString()) == 2) {
          v.dayDiff = '前天';
        } else v.dayDiff = RestAPI.timeFn(date.toLocaleDateString())+'天前';
        // console.log(date.getDay());
        var weekday = RestAPI.getWeekday(date.getDay());
        v.weekday = weekday;
        v.day = M + '-' + D;
        v.date = `${v.dayDiff}   ${v.day}  ${v.weekday}`
        _this.fastNews.splice(_this.fastNews.length, 0, v)
      }
      var map = {}, dest = [];
      for (var i = 0; i < _this.fastNews.length; i++) {
        var ai = _this.fastNews[i];
        if (!map[ai.date]) { //依赖分组字段可自行更改！
          dest.push({
            date: ai.date, //依赖分组字段可自行更改！
            data: [ai]
          });
          map[ai.date] = ai; //依赖分组字段可自行更改！
        } else {
          for (var j = 0; j < dest.length; j++) {
            var dj = dest[j];
            if (dj.date == ai.date) { //依赖分组字段可自行更改！
              dj.data.push(ai);
              break;
            }
          }
        }
      }
      _this.fastNewsList = dest;
      if (newsData == null || newsData.length < 20) {
        _this.loadOver = false;
      } else {
        _this.pageNum++;
      }
    }, function (err) {
      console.log(err);
      }, openID)
  }

  @computed get formattedFastList() {
    return this.fastNewsList.slice();
  }

  //爆料数据
  getTipsterListData = () => {
    var _this = this;
    var param = { data: { page: this.tipsterPage, limit: 20 } }
    RestAPI.invoke('/applet/getTopicList', param, function (res) {
      wx.hideToast();
      if (res.code == 200) {
        console.log(res);
        for (var v of res.data) {
          var date = new Date(v.createdAt * 1000);
          v.time = date.toLocaleDateString().replace(/\//g, "-")+" "+date.toTimeString().substr(0, 5)
          _this.tipster.splice(_this.tipster.length, 0, v);
        }
        _this.tipsterList = _this.tipster;
        if (res.data == null || res.data.length < 20) {
          _this.tipsterOver = false;
        } else {
          _this.tipsterPage++;
        }
      } else {
        console.log(res.message)
      }
    }, function (err) {
      console.log(err);
    })
  }

  @computed get formattedTipsterList() {
    return this.tipsterList.slice();
  }

  //我的收藏数据
  getColllectListData = () => {
    wx.showToast({
      title: '我的收藏数据加载中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var _this = this;
    var param = { data: { page: this.collectPage, limit: 20 } }
    RestAPI.execute('/user/getFavoriteList', param, function (res) {
      console.log('-----------',res);
      if (res.code == 200) {
        wx.hideToast();
        for (var v of res.data) {
          var date = new Date(v.time * 1000);
          v.time = date.toTimeString().substr(0, 5)
          _this.collectList.splice(_this.collectList.length, 0, v);
        }
        if (res.data == null || res.data.length < 20) {
          _this.collectOver = false;
        } else {
          _this.collectPage++;
        }
      } else {
        wx.showToast({
          title: JSON.parse(res).message,
          icon: 'none',
          duration: 3000
        })
        console.log(res.message)
      }
    }, function (err) {
      console.log(err);
    })
  }

  @computed get formattedCollectList() {
    return this.collectList.slice();
  }
}
export default new homeStore()