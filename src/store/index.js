/*
* 所有的stores都放到这里来整合吧
* */
import homeStore from './home';
import newsDetailStore from './newsDetail';
import searchStore from './searchStore.js';
import bindPhoneStore from './bindPhoneStore.js';
import userStore from './user';

const stores = {
    homeStore,
    newsDetailStore,
    searchStore,
    bindPhoneStore,
    userStore,
}

export default stores;
