import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/mobx'
import Index from './pages/home'
import stores from './store/index'
import 'taro-ui/dist/style/index.scss' // 全局引入一次即可
import '@tarojs/async-await'
import './app.scss'


// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }



class App extends Component {

  config = {
    pages: [
      'pages/home/index', //首页
      'pages/tipster/index',//爆料
      'pages/user/index', //我的
      'pages/detail/index', // 推荐&&爆料 详情
      'pages/search/index', // 搜索
      'pages/user/bind-phone/index', // 手机绑定
      'pages/user/feedback/index', //留言反馈
      'pages/user/about/index', //关于我们
      'pages/user/helps/index', //帮助中心
      'pages/user/collects/index', //我的收藏
      'pages/share/index', //分享
      
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '火星传媒',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: "#666",
      selectedColor: "#607ee2",
      backgroundColor: "#fafafa",
      borderStyle: 'black',
      list: [{
        pagePath: "pages/home/index",
        iconPath: "./assets/images/tab-bar/home.png",
        selectedIconPath: "./assets/images/tab-bar/home_active.png",
        text: "首页",
        selectedColor:''
      }, 
      {
        pagePath: "pages/tipster/index",
        iconPath: "./assets/images/tab-bar/baoliao.png",
        selectedIconPath: "./assets/images/tab-bar/baoliao_active.png",
        text: "爆料"
      },
      {
        pagePath: "pages/user/index",
        iconPath: "./assets/images/tab-bar/me.png",
        selectedIconPath: "./assets/images/tab-bar/me_active.png",
        text: "我的"
      }]
    }
  }

  componentDidMount() { }

  componentDidShow() { }

  componentDidHide() { }

  componentDidCatchError() { }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={stores}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
