import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Swiper, SwiperItem, Image, ScrollView } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTabs, AtTabsPane, AtActivityIndicator } from 'taro-ui'
import { getWindowHeight } from '@utils/style'
import Banner from './banner'
import RecommendList from '../items/recommend-list'
import FastList from '../items/fast-list'
import TipsterList from '../items/tipster-list'

import './index.scss'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight - 50}px`

@inject('homeStore')
@observer
class Home extends Component {

  constructor() {
    super(...arguments)
    this.state = {
      current: 0,

    }
  }
  config = {
    navigationBarTitleText: '火星传媒'
  }

  handleClick(value) {
    this.setState({
      current: value
    })
  }
 //小程序转发
  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '火星传媒',
      path: '/pages/home/index'
    }
  }

  componentWillMount() { }

  componentWillReact() {
  }

  componentDidMount() {
    this.store = this.props.homeStore;
    this.store.getBannerList();
    this.store.getNewListData();
    this.store.getFastNewListData();
    this.store.getTipsterListData();

  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

//推荐
  loadMoreNewsData = (e) => {
    if (this.props.homeStore.newsOver)
      this.props.homeStore.getNewListData();
  }

//快讯
  loadMoreFastData = (e) =>{
    if (this.props.homeStore.loadOver)
      this.props.homeStore.getFastNewListData();
  }

  //爆料
  loadMoreTipster = (e) =>{
    if (this.props.homeStore.tipsterOver)
      this.props.homeStore.getTipsterListData();
  }

  //推荐
  refreshNewsData = (e) => {
    wx.showToast({
      title: '刷新数据...',
      icon: 'loading',
      duration: 60 * 1000
    })
    this.props.homeStore.newsPage = 1;
    this.props.homeStore.newsList = [];
    this.props.homeStore.news = [];
    this.props.homeStore.newsOver = true;
    this.props.homeStore.getNewListData();
  }

  //快讯
  refreshFastData = (e) => {
    wx.showToast({
      title: '刷新数据...',
      icon: 'loading',
      duration: 60 * 1000
    })
    this.props.homeStore.pageNum = 1;
    this.props.homeStore.fastNewsList = [];
    this.props.homeStore.fastNews = [];
    this.props.homeStore.loadOver = true;
    this.props.homeStore.getFastNewListData();
  }

  //爆料
  refreshTipster = (e) => {
    wx.showToast({
      title: '刷新数据...',
      icon: 'loading',
      duration: 60 * 1000
    })
    this.props.homeStore.tipsterPage = 1;
    this.props.homeStore.tipsterList = [];
    this.props.homeStore.tipster = []
    this.props.homeStore.tipsterOver = true;
    this.props.homeStore.getTipsterListData();
  }


  render() {

    


    //console.log('高度',Taro.getSystemInfoSync())
    const tabList = [
      { title: '推荐' }, { title: '快讯' }, 
      { title: '爆料' }
    ]
    const { homeStore: { bannerList, formattedBannerList, formattedNewsList, isOpened, formattedFastList} } = this.props
    return (<AtTabs current={this.state.current} tabList={tabList} onClick={this.handleClick.bind(this)}>
      <AtTabsPane current={this.state.current} index={0} >
        {/* 推荐 */}
        <ScrollView
          className='container'
          style={{ height: tabInnerHeight }}
          upperThreshold={20} onScrollToUpper={this.refreshNewsData.bind(this)}
          scrollY onScrollToLower={this.loadMoreNewsData.bind(this)} lowerThreshold={20}
        >
          <Banner list={formattedBannerList} />
          <RecommendList type={3} sourceID={1} source={1}/>
        </ScrollView>

      </AtTabsPane>
      <AtTabsPane current={this.state.current} index={1}>
        {/* 快讯 */}
        <ScrollView
          className='container'
          style={{ height: tabInnerHeight }}
          upperThreshold={20} onScrollToUpper={this.refreshFastData.bind(this)}
          scrollY onScrollToLower={this.loadMoreFastData.bind(this)} lowerThreshold={20}
        >
          <FastList list={formattedFastList} sourceID={1}/>
        </ScrollView>
      </AtTabsPane>
      <AtTabsPane current={this.state.current} index={2}>
        <ScrollView
          className='container'
          style={{ height: tabInnerHeight }}
          upperThreshold={20} onScrollToUpper={this.refreshTipster.bind(this)}
          scrollY onScrollToLower={this.loadMoreTipster.bind(this)} lowerThreshold={20}
        >
          <TipsterList sourceID={1}/>
        </ScrollView>

      </AtTabsPane> 
    </AtTabs>

    )
  }
}

export default Home 
