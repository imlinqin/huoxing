import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { observer } from '@tarojs/mobx'
import './index.scss'
import { IconFont } from '@components'


@observer
export default class SwiperBanner extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      listData: this.props.list,
    }
  }
  static defaultProps = {
    list: [{
      "imgUrl": "http://uat.marschinalink.com/utils/getImg?path=/upload/731b0636c76d541b28185f7005027892.jpg",
      "createdAt": 1555050916,
      "positionType": 0,
      "bannerId": 65,
      "linkUrl": "https://uatmobile.marschinalink.com/newDetail/35716",
      "linkType": 1,
      "sortBy": 1,
      "title": "火星区块链首次亮相“币安周国际峰会”",
      "categoryId": 1,
      "status": "1",
      "updatedAt": 1555509623
    },{
      "imgUrl": "http://img8.yiqifei.com/20190509/66666eac04ca4025a93eb035e2e2614f.jpg!650w",
      "createdAt": 1555050916,
      "positionType": 0,
      "bannerId": 65,
      "linkUrl": "https://uatmobile.marschinalink.com/newDetail/35716",
      "linkType": 1,
      "sortBy": 1,
      "title": "火星区块链首次亮相“币安周国际峰会”",
      "categoryId": 1,
      "status": "1",
      "updatedAt": 1555509623
    },]
  }

  handleClick = () => {
    Taro.navigateTo({
      url: `/pages/search/index?sourceID=${2}`
    })
  }
  bannerClicl = (id) =>{
    Taro.navigateTo({
      url: `/pages/detail/index?newsId=${id}&type=1`
    })
  }

  componentDidMount() { 
    this.setState({
      listData:this.props.list
    })
  }

  render() {
    const { list } = this.props
    return (
      <View className='home-banner'>
        <Swiper
          className='home-banner__swiper'
          circular
          autoplay
          indicatorDots
          indicatorActiveColor='rgb(178, 42, 49)'
        // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
        // previousMargin
        // nextMargin
        >
          {this.props.list.map((item, key) => (
            <SwiperItem
              key={key}
              className='home-banner__swiper-item'
            >
              <Image
                className='home-banner__swiper-item-img'
                src={item.imgUrl}
                onClick={() => this.bannerClicl(item.newsID)}
              />
             
            </SwiperItem>
          ))}
        </Swiper>
        {/* 搜索入口 */}
        <View className='search' onClick={this.handleClick}>
          <IconFont icon={'0xe7c7'} styles={{fontSize:40,color:'#fff'}} />
        </View>
      </View>
    )
  }
}
