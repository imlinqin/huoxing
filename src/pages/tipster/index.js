import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Swiper, SwiperItem, Image, Checkbox, } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtImagePicker } from 'taro-ui'
import './index.scss'
import B1 from '../../images/b1.jpg'
import B2 from '../../images/b2.jpg'
import { IconFont } from '@components'
import { RestAPI } from '../../utils/utils.js';

@observer
export default class Home extends Component {

  constructor() {
    super(...arguments)
    this.state = {
      value: '',
      files: [{
        url: 'http://img11.yiqifei.com/EAN%5C672190%5Cb6aff5f438efaf8d6246705df7ba29e0.jpg',
      },
      {
        url: 'http://img11.yiqifei.com/EAN%5C672190%5Cb6aff5f438efaf8d6246705df7ba29e0.jpg',
      },
      {
        url: 'http://img11.yiqifei.com/EAN%5C672190%5Cb6aff5f438efaf8d6246705df7ba29e0.jpg',
      },
      {
        url: 'http://img11.yiqifei.com/EAN%5C672190%5Cb6aff5f438efaf8d6246705df7ba29e0.jpg',
      }
      ]
    }
  }
  config = {
    navigationBarTitleText: '发布爆料'
  }

  handleClick(value) {
    this.setState({
      current: value
    })
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }


  //小程序转发
  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '火星传媒-爆料',
      path: '/pages/tipster/index'
    }
  }

  componentDidShow() {
    var info = wx.getStorageSync("USERINFO");
    if (info == null || info == '') {
      wx.showToast({
        title: '账号未登录，无法发布爆料!',
        icon: 'none',
        duration: 2000
      });
    }
    
   }

  componentDidHide() { }

  handleInput = (e) => {
    this.setState({
      value: e.detail.value
    })
  }

  onChange(files) {
    this.setState({
      files
    })
  }
  onFail(mes) {
    console.log(mes)
  }
  onImageClick(index, file) {
    console.log(index, file)
  }

  sendTipster = () =>{
    var _this = this;
    var info = wx.getStorageSync("USERINFO");
    if (info == null || info == '') {
      wx.showToast({
        title: '账号未登录，无法发布爆料内容!',
        icon: 'none',
        duration: 3000
      });
      return;
    }
    info = JSON.parse(info);
    if (info.token==null || info.token==''){
      wx.showToast({
        title: '未关联账号，需先手机关联！',
        icon: 'none',
        duration: 3000
      });
      Taro.navigateTo({
        url: `/pages/user/bind-phone/index`
      })
      return;
    }
    wx.showToast({
      title: '爆料内容上传中...',
      icon: 'loading',
      duration: 60*1000
    })
    var param = { data: { content:this.state.value}}
    RestAPI.execute('/interaction/publishAsk', param, function (res) {
      if (res.code != 200){res = JSON.parse(res)}
      if(res.code == 200){
        _this.state.value = '';
        wx.showToast({
          title: '爆料发布成功',
          icon: 'success',
          duration: 2000
        });
        _this.setState({});
      } else if (res.code == 401) {
        Taro.navigateTo({
          url: `/pages/user/bind-phone/index`
        })
      }else{
        wx.showToast({
          title: JSON.parse(res).message,
          icon: 'none',
          duration: 3000
        })
      }
    }, function (err) {
      console.log(err);
    })
  }

  render() {
    return (<View className='container'>
      <View className='tipster'>
        <View className='tipster__inner'>
          <View className='tipster__title'>
            <Text className='tipster__title-text'>爆料内容</Text>
          </View>
          <View className='tipster__textarea-box'>
            <Textarea
              value={this.state.value}
              className='tipster__textarea'
              onInput={this.handleInput}
              type='text' placeholder='请输入爆料内容' />
          </View>
          {/* <View className='tipster__title'>
            <Text className='tipster__title-text'>添加图片</Text>
          </View>

          <View className='tipster__img-list'>
            <AtImagePicker
              files={this.state.files}
              onChange={this.onChange.bind(this)}
            />

          </View> */}
        </View>
        {/* 匿名 */}
        {/* <View>
          <Checkbox className='tipster__checkbox' value={'12'} checked={true}>匿名</Checkbox>
        </View> */}
        {/* 按钮 */}
        <View className='confirm-button' onClick={this.sendTipster.bind(this)}>
          <Text className='confirm-button__text'>确定</Text>
        </View>
      </View>
    </View>
    )
  }
}

