import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Swiper, SwiperItem, Image, Checkbox, } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtImagePicker } from 'taro-ui'
import './index.scss'
import { IconFont } from '@components'
import { RestAPI } from '../../utils/utils.js';
import qrcode from '@assets/images/qrcode.jpg';
import Logo from '@assets/images/huoxing_logo.png'

@observer
export default class Share extends Component {

    constructor() {
        super(...arguments)
        this.state = {

        }
    }
    config = {
        navigationBarTitleText: '分享'
    }



    componentWillMount() { }

    componentWillReact() {

    }

    componentDidMount() { }




    render() {
        return (<View className='container'>
            <View className='share'>
                <View className='share__logo-box'>
                    <Image className='share__logo-img' src={Logo} />
                    <Text className='share__logo-name'>火星传媒</Text>
                    <Text className='share__logo-desc'>想了解区块链，就上火星传媒</Text>
                </View>
                {/* 文章 日期 */}
                <View className='share__article-date'>
                    <Text className='share__article-date-text' >05月26日</Text>
                    <Text className='share__article-date-text'>星期四</Text>
                    <Text className='share__article-date-text'>10:23</Text>
                </View>
                {/* 文字标题 */}
                <View className='share__article-title'>
                    <Text className='share__article-title-text'>
                        如果再增加一个View，由于空间不足它会展示不全。这时可以使用flexWrap属性，它可以支持自动换行展示。
                    </Text>
                </View>
                {/* 文章内容 */}
                <View className='share__article-content'>
                    <Text className='share__article-content-text'>
                        如果再增加一个View，由于空间不足它会展示不全。这时可以使用flexWrap属性，它可以支持自动换行展示。
                    </Text>
                </View>
                {/* 火星二维码 */}
                <View className='share__qrcode' >
                    <View className='share__qrcode-img-box'><Image className='share__qrcode-img' src={qrcode} /></View>
                    <View className='share__qrcode-info'>
                        <Text className='share__qrcode-title'>火星传媒 App</Text>
                        <Text className='share__qrcode-desc' >扫码下载火星传媒App，{'\n'}了解更多有趣咨询</Text>
                    </View>
                </View>
                {/* 微信分享 */}
                <View className='share-footer__handle'>
                    <IconFont icon={'0xe684'} styles={{ fontSize: 80, color:'#86c971'}} />
                    <Text className='share-footer__handle-text'>微信好友</Text>
                </View>
            </View>
        </View>
        )
    }
}

