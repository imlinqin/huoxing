import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, RichText } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { IconFont } from '@components'

import './index.scss'
import B1 from '../../images/b1.jpg'
import B2 from '../../images/b2.jpg'
import Comment from '../items/comment'
import RecommendList from '../items/recommend-list'
import CommentFooter from '../items/comment-footer'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight}px`
// 详情图片 宽度 加上 max-width = 100%
function handleImgMaxWidth(content) {
    
    if (content !=null  && content.length > 0){
       content =   content.replace(/<img[^>]*>/gi, function (match, capture) {
           match = match.replace(/width\s*?=\s*?([‘"])[\s\S]*?\1/ig, '') //去掉宽度
           match = match.replace(/height\s*?=\s*?([‘"])[\s\S]*?\1/ig, '')//去掉高度
           match = match.replace(/<img/, '<img style="max-width:100%;height:auto; "') //添加属性
           return match
           // return match.replace(/style\s*?=\s*?([‘"])[\s\S]*?\1/ig, '')
        })
        return formatLabel(content)
    }
    //console.log('content1', content)
    return content
}
//定义 P strong 标签样式
function formatLabel(content) {
    const regexP = new RegExp('<p', 'gi');
    const regexS = new RegExp('<strong', 'gi');
    content = content.replace(regexS, `<strong style="margin-bottom:30px;display:block;color:#3d424c;line-height:28px"`);
    content = content.replace(regexP, `<p style="margin-bottom:30px;color:#3d424c;line-height:28px;font-size:16px"`);
    return content ;
}

@inject('newsDetailStore')
@observer
class Detail extends Component {

    constructor() {
        super(...arguments)
        this.store = this.props.newsDetailStore;
        this.store.commType = 1;
        this.store.isCollect = false;
        this.state = {
            isTipster: this.$router.params.type || 1
        }
    }
    static defaultProps = {
        //type: 0 //  1 文章  0 爆料 
    }
    config = {
        navigationBarTitleText: '详情'
    }



    componentWillMount() {
      this.store.newsId = (this.$router.params).newsId;
      this.store.type = (this.$router.params).type;
      this.store.commPage = 1;
      this.store.commList = [];
      this.store.type == 1 ? this.store.getNewsDetailByID() : this.store.getTipsterDetail();
      this.store.getNewsCommentList();
    }

    componentWillReact() {
    }



    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }

    //小程序转发
    onShareAppMessage(res) {
        if (res.from === 'button') {
           
        }
        let title =  this.store.newsInfo.title
        let url = this.state.isTipster == 1 ? `/pages/detail/index?newsId=${this.store.newsInfo.newsId}&type=1` : `/pages/detail/index?newsId=${this.store.newsInfo.newsId}&type=2`
        return {
            title: title,
            path: url
        }
    }

    //推荐文章分享
    handleShareClick = (id) => {
        Taro.navigateTo({
            url: `/pages/share/index?id=1`
        })
    }

    //爆料
    loadMoreCommData = (e) => {
      if (this.props.newsDetailStore.commOver)
        this.props.newsDetailStore.getNewsCommentList();
      // else
      //   wx.showToast({
      //     title: '数据全部加载完毕！',
      //     icon: 'none',
      //     duration: 3000
      //   })
    }

    render() {
        const { newsDetailStore: { newsInfo } } = this.props
        return (<View className='container'>
            <ScrollView
                className='detail'
                style={{ height: tabInnerHeight }}
                scrollY onScrollToLower={this.loadMoreCommData.bind(this)} lowerThreshold={20}
            >
                {/* 封面图 */}
            {/* {newsInfo.coverImg?
                <View className='detail__cover-img-wrap'>
                    <Image className='detail__cover-img' src={newsInfo.coverImg} />
                </View>
                :null} */}
                {/* 文章信息 */}
                <View className='detail__info-wrap'>
                    <View className='detail__title'>
                        <Text className='detail__title-text'>{newsInfo.title}</Text>
                    </View>
                    {/* 文章其他信息 */}
              {this.store.type == 1?
                    <View className='detail__info-attr'>
                        <View>
                            <Text className='detail__info-text'>{newsInfo.source + "  "}</Text>
                            <Text className='detail__info-text'>{newsInfo.time}</Text>
                        </View>
                        <View>
                                <IconFont icon={'0xe7c0'} styles={{ color: '#d2d2d2', marginRight: '5px', fontSize: 28 }} />
                                <Text className='detail__info-text'>{newsInfo.readCountStr}</Text>
                        </View>
                    </View>:null}
                    {/* 内容  载入 html标签内容 ？？ */}
                    <View className='detail__content'>
                        <RichText nodes={handleImgMaxWidth(newsInfo.content)} />
                         
                        {/* <Text>{newsInfo.content}</Text> */}
                    </View>
                    {/* 文章推荐 模块 爆料页面隐藏 */}
                    {/* {this.state.isTipster == 1 && <View className='detail__other-article'>
                        {[1,2].map((v,i)=>{
                            return <RecommendList taroKey={i}  />
                        })}
                    </View>} */}
                    {/* 评论 模块  分文章和爆料评论   tipster */}
                    {<View className='detail__comment'>
                        <Comment />
                    </View>}
                </View>
            </ScrollView>
            {/* 爆料分享 按钮 */}
            {/*this.state.isTipster == 2 && <View className='tipster-share' onClick={this.handleShareClick.bind(this)}>
                <IconFont icon={'0xe759'} styles={{ fontSize: 60, color: '#fff' }} />
                <Text className='tipster-share__text'>分享</Text>
            </View>}*}
            {/* 评论栏 */}
            <CommentFooter tipster={this.state.isTipster == 2} />
        </View>
        )
    }
}

export default Detail 
