import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Input, Textarea } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { IconFont } from '@components'
import { AtTextarea } from 'taro-ui'

import './index.scss'
import B1 from '../../images/b1.jpg'
import B2 from '../../images/b2.jpg'
import Comment from '../items/comment'
import RecommendList from '../items/recommend-list'


const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight}px`


@inject('newsDetailStore')
@observer
export default class CommentFooter extends Component {

    constructor() {
        super(...arguments)
        //this.store = this.props.newsDetailStore; 不支持？？
        this.state = {
            isComment: false, //打开评论框

            value: ''
        }
        
    }

    static defaultProps = {
        tipster: false // 爆料隐藏收藏&&分享显示
    }




    componentWillMount() { }

    componentWillReact() {
    }

    componentDidMount() {

       // this.getIsReplyUser();
        const { newsDetailStore: { replyIuputValue, replyUser, } } = this.props

        if (replyUser && replyUser.nikename) {
            //this.store.getReplyInputValue(e.detail.value)

            if (this.inputRef) {
                console.log('nikename', this.store.replyUser.nikename)
                console.log('inputRef', this.inputRef)
                this.inputRef.focus = true;
                this.inputRef.placeholder = `回复 ${replyUser.nikename}:`;
            }

        }
       

    }




    //点击底部输入栏打开评论框
    // InputOnFocus = () => {
    //     this.setState({
    //         isComment: true
    //     })
    //     setTimeout(() => {
    //         this.extarea.focus
    //     }, 500)
    // }
    //关闭评论框
    TextareaOnBlur = () => {

        this.setState({
            isComment: false
        })
    }

    handleInput = (e) => {
        this.store = this.props.newsDetailStore;
        console.log('e', e)
        // this.setState({
        //     value: e.detail.value
        // })
        this.store.getReplyInputValue(e.detail.value)
    }
    //处理留言发布信息
    handleSendBtn = () => {
      if (!this.store.replyIuputValue) {
          this.inputRef.focus = true;
          return;
      }
      var info = wx.getStorageSync("USERINFO");
      if (info == null || info == '') {
        wx.showToast({
          title: '账号未登录，无法查看收藏内容!',
          icon: 'none',
          duration: 2000
        });
        return;
      }
      info = JSON.parse(info);
      if (info.token == null || info.token == '') {
        wx.showToast({
          title: '评论或者回复需要手机验证！',
          icon: 'none',
          duration: 3000
        });
        Taro.navigateTo({
          url: `/pages/user/bind-phone/index`
        })
        return;
      }
      this.store = this.props.newsDetailStore;
      if (this.store.replyUser.id != null && this.store.replyUser.id != ''){
        this.store.replyNews();
      }else{
        this.store.commentNews();
      }
    }

    toCollect = () =>{
      this.store = this.props.newsDetailStore;
      this.store.toCollect();
    }
  

    inputFocus = () => {
        this.cat.focus = true
    }
    handleOnFocus = () => {
        this.store = this.props.newsDetailStore;
        if(!this.store.commentState){
            this.store.commentState = true
        } 
    }
    handleOnBlur = () => {
        // if (this.store.commentState) {
        //     this.store.commentState = false
        // } 

        this.store.commentState = false
    }
    //推荐文章分享
    handleShareClick = (id) => {
        Taro.navigateTo({
            url: `/pages/share/index?id=1`
        })
    }




    render() {
        let isComment = this.state.isComment
      const { newsDetailStore: { replyIuputValue, replyUser, commentState,isCollect } } = this.props
        return (
            <View className='comment-footer'
            //style={{ height: isComment ? 'auto' : Taro.pxTransform(100) }}
            >
                {true && <View className='comment-footer__bar'>
                    <View className='comment-footer__input-box'>
                        {/* <View className='comment-footer__input' onClick={() => this.InputOnFocus()}>
                            <Text className='comment-footer__input-text'>发表您的评论</Text>
                        </View> */}

                        <Input
                            ref={(node) => this.inputRef = node}
                            focus={commentState}
                            value={replyIuputValue}
                            onFocus={this.handleOnFocus}
                            onInput={this.handleInput}
                            onBlur={this.handleOnBlur}
                            className='comment-footer__input' type='text' 
                            placeholder={ replyUser && replyUser.nikename ? `回复 ${replyUser.nikename}:` :'发表您的评论'} />
                    </View>
                    {/* 当评论状态打开时 收藏&& 分享暂隐藏 */}
                    {!this.props.tipster && !commentState && <View className='comment-footer__handle'>
                <View className='comment-footer__handle-box' onClick={this.toCollect.bind(this)}>
                  <IconFont icon={isCollect ? '0xe7cc' : '0xe7cd'} styles={{ color: isCollect ? '#6083e3' : '#999', fontSize: 40 }} />
                  <Text className='comment-footer__handle-text'>{isCollect ? '已收藏' : '收藏'}</Text>
                        </View>
                        <View className='comment-footer__handle-box' onClick={this.handleShareClick.bind(this)}>
                            <IconFont icon={'0xe759'} styles={{ color: '#999', fontSize: 40 }} />
                            <Text className='comment-footer__handle-text'>分享</Text>
                        </View>
                    </View>}
                    {/* 发送按钮 */}
                    {commentState && <View className='comment-footer__send-button' onClick={()=>{this.handleSendBtn()}}>
                        <Text className='comment-footer__send-button-text' style={{ color: this.state.value ? '#333' : '#999' }}>发送</Text>
                    </View>}

                </View>}
                {/* 弹出层评论框 由于小程序性能造成卡顿已抛弃 */}
                {/* {true ? '' : <View className='comment-area'>
                    <View className='comment-area__header'>
                        <View onClick={this.TextareaOnBlur}>
                            <Text className='comment-area__header-text'>取消</Text>
                        </View>
                        <View onClick={this.TextareaOnBlur}>
                            <Text className='comment-area__header-text'>发布</Text>
                        </View>
                    </View>
                    <View className='comment-area__body'>
                        <AtTextarea
                            className='comment-area__textarea'
                            count={false}
                            height={200}
                            value={this.state.value}
                            onChange={this.handleChange.bind(this)}
                            maxLength={200}
                            autoFocus
                            placeholder='请输入留言内容'
                        />
                    </View>
                </View>} */}
            </View>
        )
    }
}


