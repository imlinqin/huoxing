import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { IconFont } from '@components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('homeStore')@inject('searchStore')
@observer
export default class TipsterList extends Component {
  static defaultProps = {
    list: [{
      "bearishCount": 35,
      "bullishCount": 63,
      "categoryId": "0",
      "content": "据火币全球站数据，截至5月11日16：40（GMT+8），NEW目前涨幅超过18%，现报0.003997USDT。据悉，有“电商复仇者联盟”之称的NEW于新加坡时间4月16日20:00（GMT+8）通过火币优选上币通道Huobi Prime上线火币主板，其基于NewChain、NewPay的链商应用软件NewMall已正式上线，NewPay累计注册人数4月突破20万。",
      "createdAt": 1557567832,
      "evaluateType": 0,
      "imgUrl": "",
      "isComment": 1,
      "isFlash": "1",
      "isPush": 0,
      "language":
        "zh-CN",
      "newsId": 38840,
      "orderBy": "1",
      "publish": 1, "pushTime": 0,
      "readCount": 15322, "serverAt": 1557568769,
      "shareCount": 0,
      "shortBrief": "", "source": "",
      "status": 1, "tag": "",
      "title": "NEW今日上涨超18%，NewPay累计注册人数4月突破20万", "top": 0, "type": 0, "userId": 17
    }]
  }

  handleClick = (id) => {
    Taro.navigateTo({
      url: `/pages/detail/index?newsId=${id}&type=2`
    })
  }

  render() {
    const { list } = this.props
    const { homeStore: { formattedTipsterList } } = this.props;
    const { searchStore: { formattedSeachTipsterList } } = this.props;
    return (<View className='list'>
      {(this.props.sourceID == 2 ?formattedSeachTipsterList:formattedTipsterList).map((v, i) => {
        return <View taroKey={i} className='list__Item' onClick={this.handleClick.bind(this, v.newsId)}>
          {/* 用户信息 */}
          {/* <View className='list__user'>
            <Image className='list__user-avatar' src='https://img2.yiqifei.com/20180928/de3a99a67e7c433e9b9c828ed3df609c.jpg!80' />
            <Text className='list__user-name'>{v.author}</Text>
          </View> */}
          <View className='list__info-box'>
            <View className='list__title'>
              <Text className='list__title-text list__title-text--big'>{v.title}</Text>
            </View>
            {/* 描述 */}
            <View className='list__desc'>
              <Text className='list__desc-text'>{v.content}</Text>
            </View>
            <View className='list__info'>
              <View className='list__date'>
                <Text className='list__info-text'>{v.time}</Text>
              </View>
              <View className='list__handle-box'>
                <View>
                  <IconFont icon={'0xe606'} styles={{ color: '#d2d2d2', marginRight: '5px', fontSize: 28 }} />
                  <Text className='list__info-text'>{v.commentCount}</Text>
                </View>
                
              </View>

            </View>
          </View>

        </View>
      })}
    </View>)
  }
}
