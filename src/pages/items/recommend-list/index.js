import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { IconFont } from '@components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('homeStore') @inject('searchStore')
@observer
export default class RecommendList extends Component {
  static defaultProps = {
    type: 1,// 1:小图 2:大图 3:小大图混合展示
    list: [{
      "bearishCount": 35,
      "bullishCount": 63,
      "categoryId": "0",
      "content": "据火币全球站数据，截至5月11日16：40（GMT+8），NEW目前涨幅超过18%，现报0.003997USDT。据悉，有“电商复仇者联盟”之称的NEW于新加坡时间4月16日20:00（GMT+8）通过火币优选上币通道Huobi Prime上线火币主板，其基于NewChain、NewPay的链商应用软件NewMall已正式上线，NewPay累计注册人数4月突破20万。",
      "createdAt": 1557567832,
      "evaluateType": 0,
      "imgUrl": "",
      "isComment": 1,
      "isFlash": "1",
      "isPush": 0,
      "language":
        "zh-CN",
      "newsId": 38840,
      "orderBy": "1",
      "publish": 1, "pushTime": 0,
      "readCount": 15322, "serverAt": 1557568769,
      "shareCount": 0,
      "shortBrief": "", "source": "",
      "status": 1, "tag": "",
      "title": "NEW今日上涨超18%，NewPay累计注册人数4月突破20万", "top": 0, "type": 0, "userId": 17
    }]
  }

  handleClick = (id) => {
    Taro.navigateTo({
      url: `/pages/detail/index?newsId=${id}&type=1`
    })
  }

  render() {
    const { list, type } = this.props
    const { homeStore: { formattedNewsList, formattedCollectList } } = this.props;
    const { searchStore: { formattedSeachDataList } } = this.props;


    let ViewDom = '';
    let data = this.props.sourceID == 2 ? formattedSeachDataList : this.props.source == 1 ? formattedNewsList : formattedCollectList
   
    ViewDom =  <View className='list'>
        {data.map((v, i) => {
            let ii = i + 1
            //索引 第 4 5个展示大图
          if ((type == 3 && ((i + 1) % 4 == 0 )) ) {
              return <View taroKey={i} className='list__Item list__Item--column' onClick={this.handleClick.bind(this, v.newsId)} >
                {/* 缩略图 */}
                <View className='list__thumb list__thumb--big'>
                  <Image className='list__thumb-img list__thumb-img--big' src={v.imgUrl} />
                </View>
                <View className='list__info-box'>
                  <View className='list__title'>
                    <Text className='list__title-text list__title-text--big'>{v.title}</Text>
                  </View>
                  {/* 描述 */}
                  <View className='list__desc'>
                    <Text className='list__desc-text'>{v.shortBrief}</Text>
                  </View>
                  <View className='list__info list__info--big'>
                    <View>
                      <Text className='list__info-text'>{v.source + "  "}</Text>
                      <Text className='list__info-text'>{v.time}</Text>
                    </View>
                    <View>
                      <IconFont icon={'0xe7c0'} styles={{ color: '#d2d2d2', marginRight: '5px', fontSize: 28 }} />
                      <Text className='list__info-text'>{v.readCountStr}</Text>
                    </View>
                  </View>
                </View>

              </View>
            }
            else if (type == 1 || type == 3) {
              return <View taroKey={i} className='list__Item ' onClick={this.handleClick.bind(this, v.newsId)}>
                <View className='list__info-box'>
                  <View className='list__title'>
                    <Text className='list__title-text'>{v.title}</Text>
                  </View>
                  <View className='list__info'>
                    <View>
                      <Text className='list__info-text'>{v.source + "  "}</Text>
                      <Text className='list__info-text'>{v.time}</Text>
                    </View>
                    <View>
                      <IconFont icon={'0xe7c0'} styles={{ color: '#d2d2d2', marginRight: '5px', fontSize: 28 }} />
                      <Text className='list__info-text'>{v.readCountStr}</Text>
                    </View>
                  </View>
                </View>
                {/* 缩略图 */}
                <View className='list__thumb '>
                  <Image className='list__thumb-img list__thumb-img' src={v.imgUrl} />
                </View>
              </View>
            }
            
          })

        }
      </View>
     
   

    return ViewDom
  }
}
