import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, ScrollView } from '@tarojs/components'
import { IconFont } from '@components'
import { observer, inject } from '@tarojs/mobx'

import './index.scss'
@inject('homeStore') @inject('searchStore')
@observer
export default class FastList extends Component {
  static defaultProps = {
    list: [{
      "bearishCount": 35,
      "bullishCount": 63,
      "categoryId": "0",
      "content": "据火币全球站数据，截至5月11日16：40（GMT+8），NEW目前涨幅超过18%，现报0.003997USDT。据悉，有“电商复仇者联盟”之称的NEW于新加坡时间4月16日20:00（GMT+8）通过火币优选上币通道Huobi Prime上线火币主板，其基于NewChain、NewPay的链商应用软件NewMall已正式上线，NewPay累计注册人数4月突破20万。",
      "createdAt": 1557567832,
      "evaluateType": 0,
      "imgUrl": "",
      "isComment": 1,
      "isFlash": "1",
      "isPush": 0,
      "language":
        "zh-CN",
      "newsId": 38840,
      "orderBy": "1",
      "publish": 1, "pushTime": 0,
      "readCount": 15322, "serverAt": 1557568769,
      "shareCount": 0,
      "shortBrief": "", "source": "",
      "status": 1, "tag": "",
      "title": "NEW今日上涨超18%，NewPay累计注册人数4月突破20万", "top": 0, "type": 0, "userId": 17
    }]
  }

  componentDidMount() {

  }

  toShare = (i,k) =>{
    // let newsInfo = {
    //   title:info.title,
    //   content:info.content,
    //   timeStr :info.day+" "+info.time,
    //   weekday:info.weekday
    // }
    Taro.navigateTo({
      url: `/pages/share/index?i=${i}&k=${k}`
    })
  }

  render() {
    const { list } = this.props;
    const { homeStore: { formattedFastList } } = this.props;
    const { searchStore: { formattedSeachFastList } } = this.props;
    const listDom = (this.props.sourceID == 2 ? formattedSeachFastList : formattedFastList).map((v, i) => {
      return <View taroKey={i}>
        {/* 日期标题 */}
        <View className='list__date-title'>
          <View className='list__date-title-icon'><IconFont icon='0xe603' styles={{ color: '#fff', fontSize: 30 }} /></View>
          <Text className='list__date-title-text'>{v.date}</Text>
        </View>
        {v.data && (v.data).map((s, k) => {
          return <View taroKey={k} className='list__info'>
            {/* 子标题 */}
            <View className='list__sub-title'>
              {/* 轴点 */}
              <View className='list__sub-title--left'>
                <View className='list__sub-title-dot'></View>
                <View><Text className='list__sub-title-text'>{s.time}</Text></View></View>


            </View>
            {/* 内容 */}
            <View className='list__info-content'>
              {/* 标题 */}
              <View className='list__info-title-box'>
                <Text className='list__info-title'>{s.title}</Text>
              </View>
             
              {/* 描述*/}
              <View className='list__info-desc-box'>
              <Text className='list__info-desc'>{s.content}</Text>
              </View>
            </View>
            {/* 利好利空 */}
            <View className='list__info-handle'>
              <View className='list__info-handle-left'>
                {/* 利好 */}
                <View className='list__info-handle-item'>
                  <IconFont icon='0xe604' styles={{ color: '#d2d2d2', fontSize: 28,marginRight:'5px' }} />
                  <Text className='list__sub-title-text'>利好{s.bullishCount}</Text>
                </View>
                {/* 利空 */}
                <View className='list__info-handle-item'>
                  <IconFont icon='0xe60f' styles={{ color: '#d2d2d2', fontSize: 28, marginRight:'5px'}} />
                  <Text className='list__sub-title-text'>利空{s.bearishCount}</Text>
                </View>
              </View>
              {/* 分享 */}
              <View className='list__sub-title-share' onClick={this.toShare.bind(this,i,k)}>
                <IconFont icon='0xe759' styles={{ color: '#d2d2d2', fontSize: 28 }} />
                <Text className='list__sub-title-text'>分享</Text>
              </View>
            </View>
          </View>
        })}
      </View>
    })




    return (<View className='list'>
      {listDom}
    </View>)
  }

  loadMoreData = () => {
    console.log("------")
    this.props.store.getNewListData();
  }
}
