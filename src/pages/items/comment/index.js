import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { IconFont } from '@components'
import './index.scss'

@inject('newsDetailStore')
@observer
export default class Comment extends Component {

    constructor() {
        super(...arguments)
        this.state = {
        }
    }




    componentWillMount() {

        this.store = this.props.newsDetailStore;
        
     }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }
    //回复用户评论
    onReply = (v) =>{
        //底部回复栏显示改用名  回复 用户名：
        this.store = this.props.newsDetailStore;
        this.store.getReplyUser(v);
    }

    render() {
      const { newsDetailStore: { commList, newsInfo } } = this.props
     // console.log('commList',commList.slice().length)
        return (<View className='comment'>
            {commList.slice().length > 0 && <View className='comment__title'>
            <Text className='comment__title-text'>{'评论'}</Text>
            </View>}
            <View className='comment__list'>
                {commList.map((v, i) => {
                    return <View taroKey={i} className='comment__item'>
                        {/*  头像 */}
                        <View className='comment__avatar'>
                            <Image className='comment__avatar-img' src={v.avatar} />
                        </View>
                        {/* 评论信息 */}
                        <View className='comment__info'>
                            <View className='comment__list-title'>
                                <Text className='comment__list-title-text'>{v.nikename}</Text>
                                {/* 回复 */}
                                <View className='comment__reply-btn' 
                                onClick={this.onReply.bind(this,v)}
                                >
                                    <IconFont icon={'0xe606'} styles={{ color: '#d2d2d2', fontSize: 40 }} />
                                    <Text className='comment__reply-btn-text' >回复</Text>
                                </View>
                            </View>
                            {/* 时间格式   刚刚     11:12 */}
                            <View className='comment__list-time'><Text className='comment__list-time-text'>{v.createdStr}</Text></View>
                            {/* 回复他人 */}
                            {v.sourceCommentId != null ?
                                <View className='comment__list-reply'>
                                    <Text className='comment__list-reply-text'>{v.sourceContent}</Text>
                                </View> : null}
                            {/* 评论内容 */}
                            <View className='comment__list-content'><Text className='comment__list-content-text'>{v.content}</Text></View>
                            {/* 回复列表 */}
                            {/* {[11,2,3].map((s,k)=>{
                                return <View taroKey={k} className='comment__list-content-reply'>
                                <Text className='comment__list-content-reply-text'>我是谁：沃尔玛你的头像这枚QQ呀试着在开发者菜单中打开Live Reload，</Text>
                            </View>
                            })} */}
                        </View>
                    </View>
                })}
            </View>
        </View>
        )
    }
}


