import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Input } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { IconFont } from '@components'
import { AtTabs, AtTabsPane, AtActivityIndicator } from 'taro-ui'
import './index.scss'
import RecommendList from '../items/recommend-list'
import FastList from '../items/fast-list'
import TipsterList from '../items/tipster-list'
const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info
const tabInnerHeight = `${windowHeight }px`
@inject('searchStore') @inject('homeStore')
@observer
export default class Search extends Component {

    constructor() {
        super(...arguments)
        this.store = this.props.searchStore;
        this.store.searchDataList = [];
        this.store.searchPage = 1;
        this.store.searchOver = false;
        this.state = {
            searchValue: '',
            current: 0,
        }
    }
    config = {
        navigationBarTitleText: '搜索'
    }

    handleClick(value) {
        this.setState({
            current: value
        })
    }

    componentWillMount() { }

    componentWillReact() {
    }

    componentDidMount() {
      this.store.seachNewsData();
        //this.store.getUserInfo();
        // this.store.getBannerList();
        // this.store.getNewListData();
        // this.store.getFastNewListData();
        // this.store.getTipsterListData();

    }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }

    

    //推荐
    loadMoreNewsData = (e) => {
      if (!this.props.searchStore.searchOver)
        this.props.searchStore.seachNewsData();
    }

    //快讯
    loadMoreFastData = (e) => {
      if (this.props.searchStore.loadOver)
        this.props.searchStore.getFastNewListData();
    }

    handleInput = (e) => {
        this.store.searchContent = e.detail.value;
        this.setState({
            searchValue: e.detail.value
        })
    }

    searchEvent = () => {
      this.store = this.props.searchStore;
      this.store.seachNewsData();
        // this.store.getNewListData();
        // this.store.getFastNewListData();
        // this.store.getTipsterListData();
    }

    render() {
        const tabList = [{ title: '推荐' }, { title: '快讯' }, { title: '爆料' }]
      const { searchStore: { formattedSeachNewsList} } = this.props
        return (<View className='container'>
            {/* 搜索栏 */}
            <View className='search-bar'>
                <View className='search-bar__input-box'>
                    <IconFont icon={'0xe7c7'} styles={{ fontSize: 36, color: '#666' }} />
                    <Input
                        value={this.state.searchValue}
                        onInput={this.handleInput}
                        placeholder='请输入关键字'
                        className='search-bar__input' />
                </View>
                <View className='search-bar__btn' onClick={this.searchEvent.bind(this)}>
                    <Text className='search-bar__btn-text'>搜索</Text>
                </View>
            </View>
            {/* 推荐 */}
            <ScrollView
                className='container'
                style={{ height: tabInnerHeight }}
                scrollY onScrollToLower={this.loadMoreNewsData.bind(this)} lowerThreshold={20}
            >
                <RecommendList type={1} sourceID={2}/>
            </ScrollView>
        </View>
        )
    }
}


