import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Swiper, SwiperItem, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import B1 from '../../images/b1.jpg'
import B2 from '../../images/b2.jpg'
import { IconFont } from '@components'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight}px`

@inject('userStore')
@observer
export default class User extends Component {

  constructor() {
    super(...arguments)
    this.store = this.props.userStore;
    this.state = {
      navListData: [
        { icon: '0xe7cc', iconColor: '#fa6b6a', title: '我的收藏', page: '/pages/user/collects/index' },
        { icon: '0xe7cb', iconColor: '#5b82ff', title: '帮助中心', page: '/pages/user/helps/index' },
        { icon: '0xe7c9', iconColor: '#68ca95', title: '留言反馈', page: '/pages/user/feedback/index' },
        { icon: '0xe7c8', iconColor: '#f1aa44', title: '关于我们', page: '/pages/user/about/index' }
      ]
    }
  }
  config = {
    navigationBarTitleText: '我的'
  }



  componentWillMount() { }

  componentWillReact() {
    console.log((this.$router.params))
    console.log('componentWillReact')
    this.setState({
    })
  }

  componentDidMount() {
    this.store.judgeLogin();
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  //小程序转发
  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '火星传媒-我的',
      path: '/pages/user/index'
    }
  }



  onBindPhoneClick = (id) => {
    Taro.navigateTo({
      url: `/pages/user/bind-phone/index`
    })
  }

  onNavListClick = (page) => {
    Taro.navigateTo({
      url: page
    })
  }

  getUserInfo  = (userInfo) =>{
    this.store.getUserInfo(userInfo)
  }

  render() {
    const { userStore: { userInfo } } = this.props
    return (<View className='container'>
      <ScrollView
        scrollY
        //className='container'
        style={{ height: tabInnerHeight }}
      >
        {/* 个人信息 */}
        {userInfo.openid != null ? <View className='user-info'>
          <View className='user-info__name'>
            <Image className='user-info__avatar' src={userInfo.avatarUrl} />
            <Text className='user-info__name-text'>{userInfo.nickName}</Text>
          </View>
        </View> :
          <View className='user-info'>
            {/* 微信登录  */}
            <Button open-type='getUserInfo' onGetUserInfo={this.getUserInfo} >
              <Text>微信用户登录</Text>
            </Button>
          </View>}
        {/* 导航 */}
        <View className='nav-list'>
          {this.state.navListData.map((v, i) => {
            return <View taroKey={i} className='nav-list__item' style={{ borderColor: this.state.navListData.length == i + 1 ? '#fff' : '#ddd' }} onClick={this.onNavListClick.bind(this, v.page)}>
              <View className='nav-list__content'>
                <IconFont icon={v.icon} styles={{ fontSize: 50, color: v.iconColor }} />
                <Text className='nav-list__title'>{v.title}</Text>
              </View>
              {/* 箭头 */}
              <View className='nav-list__arrow'>
                <IconFont icon='0xe79a' styles={{ fontSize: 40, color: '#b6b9be' }} />
              </View>
            </View>
          })}
        </View>
      </ScrollView>

    </View>

    )
  }
}

