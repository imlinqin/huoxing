import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTextarea } from 'taro-ui'
import './index.scss'
import Logo from '@assets/images/huoxing_logo.png'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight}px`

@observer
export default class About extends Component {

    constructor() {
        super(...arguments)
        this.state = {

            value: '',

        }
    }
    config = {
        navigationBarTitleText: '关于我们'
    }



    componentWillMount() { }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }





    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

    
    //小程序转发
    onShareAppMessage(res) {
        if (res.from === 'button') {
            // 来自页面内转发按钮
            console.log(res.target)
        }
        return {
            title: '火星传媒-关于我们',
            path: '/pages/user/about/index'
        }
    }




    render() {
        return (<View className='container'>
            <ScrollView
                scrollY
                className='about'
                style={{ height: tabInnerHeight }}
            >
                <View className='about__inner'>
                    <View className='about__logo-box'>
                        <Image className='about__logo-img' src={Logo} />
                        <Text className='about__logo-name'>火星传媒</Text>
                        <Text className='about__logo-desc'>想了解区块链，就上火星传媒</Text>
                    </View>
                    <View className='about__content'>
                        <Text className='about__content-text'>
                           火星站Pro小程序是由火星传媒团队运营的重要的区块链资讯传播载体，这里汇聚了最前沿的区块链行业资讯、实时快讯、圈内爆料及最新动态，让每一个有需要的人都能轻松获得想要的信息。
 
                        </Text>
                    </View>
                    <View className='about__extra'>
                        <Text className='about__extra-text'>
                            商务合作：13760285079 {'\n'}
                            我要投稿：marsmedia@marsmedia24.com{'\n'}
                            加入社群：Mars_Media1
                        </Text>
                    </View>





                </View>
            </ScrollView>
        </View>
        )
    }
}

