import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTextarea } from 'taro-ui'
import './index.scss'
import { RestAPI } from '../../../utils/utils.js';



@observer
export default class Feedback extends Component {

    constructor() {
        super(...arguments)
        this.state = {
          userInfo:{},
            value: '',

        }
    }
    config = {
        navigationBarTitleText: '留言反馈'
    }

  //小程序转发
  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '火星传媒-留言反馈',
      path: '/pages/user/feedback/index'
    }
  }


    componentWillMount() { }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { 
      var info = wx.getStorageSync("USERINFO");
      if (info != null && info != ''){
        this.state.userInfo = JSON.parse(info);
      }else{
        wx.showToast({
          title: '未登录，将无法留言反馈!',
          icon: 'none',
          duration: 2000
        })
      }
    }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }





    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

  feedBackEvent = (e) => {
    if (this.state.userInfo.token == null || this.state.userInfo.token == ''){
      wx.showToast({
        title: '账号未登录，无法留言反馈!',
        icon: 'none',
        duration: 3000
      })
      return;
    }
    wx.showToast({
      title: '反馈内容上传中...',
      icon: 'loading',
      duration: 60 * 1000
    })
    var param = { data: { content: this.state.value } }
    RestAPI.execute('/common/feedBack', param, function (res) {
      if (res.code == 200) {
        wx.showToast({
          title: '留言反馈发送成功',
          icon: 'success',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: JSON.parse(res).message,
          icon: 'none',
          duration: 3000
        })
      }
    }, function (err) {
      console.log(err);
    })
  }



    render() {
        return (<View className='container'>
            <View className='feedback'>
                <View className='feedback__title'><Text className='feedback__title-text'>留言反馈</Text></View>
                <AtTextarea
                    count={false}
                    value={this.state.value}
                    onChange={this.handleChange.bind(this)}
                    maxLength={200}
                    placeholder='请输入留言内容'
                />

            </View>
            {/* 按钮 */}
          <View className='confirm-button' onClick={this.feedBackEvent.bind(this)}>
                <Text className='confirm-button__text'>确定</Text>
            </View>
        </View>
        )
    }
}

