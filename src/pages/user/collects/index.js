import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTextarea } from 'taro-ui'
import CollectsList from '../../items/recommend-list'
import './index.scss'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info

const tabInnerHeight = `${windowHeight  + 50 }px`

@inject('homeStore')
@observer
export default class Collects extends Component {

    constructor() {
        super(...arguments)
        this.state = {
          userInfo:{},
            value: '',

        }
    }
    config = {
        navigationBarTitleText: '我的收藏'
    }



    componentWillMount() { }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { 
      var info = wx.getStorageSync("USERINFO");
      if (info == null || info == '') {
        wx.showToast({
          title: '账号未登录，无法查看收藏内容!',
          icon: 'none',
          duration: 2000
        });
        return;
      }
      info = JSON.parse(info);
      if (info.token == null || info.token == '') {
        wx.showToast({
          title: '未关联账号，需先手机关联！',
          icon: 'none',
          duration: 3000
        });
        Taro.navigateTo({
          url: `/pages/user/bind-phone/index?page=collect`
        })
        return;
      }
      this.store = this.props.homeStore;
      this.store.collectList = [];
      this.store.collectPage = 1;
      this.store.getColllectListData();
    }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }





    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

    //加载更多
    loadMoreCollectData = (e) =>{
      if (this.props.homeStore.collectOver){
        this.props.homeStore.getColllectListData();
      }else{
        wx.showToast({
          title: '数据加载完，无更多数据',
          duration: 3000
        })
      }
    }



    render() {
        return (<View className='container'>
            <ScrollView
                className='about'
                style={{ height: tabInnerHeight }}
                scrollY onScrollToLower={this.loadMoreNewsData.bind(this)} lowerThreshold={20}
            >
            <CollectsList source={4} sourceID={1}/>
            </ScrollView>
        </View>
        )
    }
}

