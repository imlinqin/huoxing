import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { RestAPI } from '../../../utils/utils.js';
import { AtInput, AtForm, AtToast } from 'taro-ui'
import './index.scss'



@inject('bindPhoneStore') @inject('userStore')
@observer
export default class BindPhone extends Component {

    constructor() {
        super(...arguments)
      this.store = this.props.bindPhoneStore;
      this.store.fromCollect = false;
      if (this.$router.params.page){
        this.store.fromCollect = true;
      }
        this.state = {
            phoneValue: '',
            verCodeValue: '',
            value6: '',
            mobile_code: this.props.mobile_code || '',
            count: 60,
            liked: true
        }
    }
    config = {
        navigationBarTitleText: '验证'
    }



    componentWillMount() { }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }


    handlePhoneInput = (e) => {
      this.store.phone = e;
        // this.setState({
        //     value: e.detail.phoneValue
        // })
    }
    handleVerCodeInput = (e) => {
        this.setState({
            value: e.detail.phoneValue
        })
    }

    handleChange(value) {
      
      this.store.verCode= value;
        this.setState({
            value
        })
        // 在小程序中，如果想改变 value 的值，需要 `return value` 从而改变输入框的当前值
        return value
    }

    getCode = () => {
      if (!RestAPI.isMPNumber(this.store.phone)) {
        wx.showToast({
          title:'手机号码输入不正确!',
          icon: 'none',
          duration: 3000
        })
        return;
      }
      this.store.getVerCode();
        if (!this.state.liked) {
            return
          }
        let count = this.state.count
        const timer = setInterval(() => {
            this.setState({ count: (count--), liked: false }, () => {
                if (count === 0) {
                    clearInterval(timer);
                    this.setState({
                        liked: true,
                        count: 60
                    })
                }
            });
        }, 1000);

    }



    render() {
      const { userStore: { userInfo } } = this.props
        return (<View className='container'>
            <AtForm className='bind-phone'>
                {/* <View className='bind-phone'> */}

                <AtInput
                    name='value6'
                    border={false}
                    title='手机号码'
                    type='phone'
                    placeholder='手机号码'
                    value={this.state.value6}
              onChange={this.handlePhoneInput.bind(this)}
                />
                <AtInput
                    style={{ borderColor: '#fff' }}
                    name='value6'
                    border={false}
                    title='验证码'
                    type='text'
                    placeholder='验证码'
                    value={this.state.value6}
                    onChange={this.handleChange.bind(this)}
                >
                    <View className='bind-phone__handle' onClick={this.getCode}>
                        { this.state.liked ? <Text className='bind-phone__handle-text'>发送验证码</Text> : 
                        <Text className='bind-phone__handle-text bind-phone__handle-liked-text ' >{this.state.count + 's' }</Text>}
                    </View>
                </AtInput>
                {/* <View className='bind-phone__item'>
                        <Text className='bind-phone__label'>手机号码</Text>
                        <Input
                            value={this.state.phoneValue}
                            onInput={this.handlePhoneInput}
                            placeholder='请输入您的手机号码'
                            className='bind-phone__input'
                        />
                    </View>
                    <View className='bind-phone__item'>
                        <Text className='bind-phone__label'>验证码</Text>
                        <Input
                            value={this.state.verCodeValue}
                            onInput={this.handleVerCodeInput}
                            placeholder='请输入您验证码'
                            className='bind-phone__input'
                        />
                        <View className='bind-phone__handle'>
                            <Text className='bind-phone__handle-text'>发送验证码</Text>
                        </View>
                    </View> */}
                {/* </View> */}
                {/* 按钮 */}

            </AtForm>
            {/* 按钮 */}
          <View className='confirm-button' onClick={(e)=>{this.store.registerEvent(userInfo);}}>
                <Text className='confirm-button__text'>确定</Text>
            </View>
        </View>
        )
    }
}

