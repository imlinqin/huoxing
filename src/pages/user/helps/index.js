import Taro, { Component } from '@tarojs/taro'
import { View, Text, Input, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtAccordion, AtList, AtListItem } from 'taro-ui'
import './index.scss'

const info = Taro.getSystemInfoSync()
const { windowHeight, statusBarHeight, titleBarHeight } = info
const tabInnerHeight = `${windowHeight}px`

@observer
export default class Helps extends Component {

    constructor() {
        super(...arguments)
        this.state = {
            open: false,
            helpsData: [
                { id: 1, open: true, title: '1：为什么我收不到短信验证码？', content: '短信网关拥堵或出现异常时会导致已发送的短信出现延时或丢失的情况，建议过段时间再尝试获取' },
                { id: 2, open: false, title: '2：怎么在小程序报料？', content: '登录火星站Pro小程序，点击底部报料按钮，填写报料内容并且等待客服审核，通过可以发布报料' },
               
                { id: 3, open: false, title: '3：发布报料有什么要求吗？', content: '发布报料进入审核流程后，如不符合平台规范（涉嫌不雅甚至恶俗，以及发布内容包含广告信息等情况）将不允通过，直至内容合规，才可通过审核。' },
                { id: 4, open: false, title: '4：意见反馈', content: '更多问题，添加客服微信：Mars_Media1进行反馈' },
            ]
        }
    }
    handleClick(id) {
        this.setState({
            helpsData: this.state.helpsData.map((item) => {
                if (item.id == id) {
                    item.open = !item.open  //改变手风琴打开状态
                }
                return item
            })
        })
    }

    //小程序转发
    onShareAppMessage(res) {
        if (res.from === 'button') {
            // 来自页面内转发按钮
            console.log(res.target)
        }
        return {
            title: '火星传媒-帮助中心',
            path: '/pages/user/helps/index'
        }
    }



    componentWillMount() { }

    componentWillReact() {
        console.log('componentWillReact')
    }

    componentDidMount() { }

    componentWillUnmount() { }

    componentDidShow() { }

    componentDidHide() { }











    render() {
        return (<View className='container'>
            <ScrollView
                scrollY
                className='helps'
                style={{ height: tabInnerHeight }}
            >
                <View className='helps__inner'>
                    {this.state.helpsData.map((v, i) => {
                        return <AtAccordion taroKey={i}
                            open={v.open}
                            onClick={this.handleClick.bind(this, v.id)}
                            title={v.title}
                        >
                            <View className='helps__content'>
                                <Text className='helps__content-text'>
                                    {v.content}
                                </Text>
                            </View>
                        </AtAccordion>
                    })}

                </View>

            </ScrollView>
        </View>
        )
    }
}

