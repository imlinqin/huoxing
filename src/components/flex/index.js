//https://mobile.ant.design/components/flex-cn/ 搬ant design flex组件
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

export default class Flex extends Component {

    static defaultProps = {
        direction: 'row',
        wrap: 'nowrap',
        justify: 'start',
        align: 'center',
        onClick: () => { },
    }
    render() {
        let { style, direction, wrap, justify, align, children, onClick } = this.props;
        console.log('children',this.props)
        let transferConst = [justify, align];
        transferConst = transferConst.map((el) => {
            let tempTxt;
            switch (el) {
                case 'start':
                    tempTxt = 'flex-start';
                    break;
                case 'end':
                    tempTxt = 'flex-end';
                    break;
                case 'between':
                    tempTxt = 'space-between';
                    break;
                case 'around':
                    tempTxt = 'space-around';
                    break;
                default:
                    tempTxt = el;
                    break;
            }
            return tempTxt;
        });
        const flexStyle = {
            flexDirection: direction,
            flexWrap: wrap,
            justifyContent: transferConst[0],
            alignItems: transferConst[1],
        };
        
        return (<View style={{display: 'flex',...flexStyle, ...style}} onClick={onClick}>
           {this.props.children}
        </View>
        );
    }
}
