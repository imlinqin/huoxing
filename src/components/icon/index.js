import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'

export default class IconFont extends Component {
  static defaultProps = {
    style:{color:'#333',fontSize:Taro.pxTransform(28)},
    icon:'0xe7c8'
  }

  render () {
    const { icon ,styles} = this.props
    return (<Text  className='iconfont' style={postcss(styles)}>{String.fromCharCode(icon)} </Text>)
  }
}


/**
 * // NOTE 样式在编译时会通过 postcss 进行处理，但 js 中的 style 可能需要运行时处理
 * 例如加 prefix，或者对 RN 样式的兼容，又或是在此处统一处理 Taro.pxTransform
 * 此处只做演示，若需要做完善点，可以考虑借助 https://github.com/postcss/postcss-js
 */
function postcss(style) {
    if (!style) {
      return style
    }
    const { fontSize, ...restStyle } = style
    const newStyle = {}
    if (fontSize && typeof(fontSize) == 'number') {
      // NOTE 如 RN 不支持 background，只支持 backgroundColor
      newStyle.fontSize = Taro.pxTransform(fontSize)
    }
    else {
        newStyle.fontSize = fontSize
    }
    return { ...restStyle, ...newStyle }
  }
