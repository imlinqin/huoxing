import Taro from '@tarojs/taro'

const baseUrl = 'https://api.marschinalink.com';
const testUrl = 'https://uat.marschinalink.com';
const week = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];
// var token = '';

export class RestAPI{
  static token = '';

  //接口请求
  static invoke(link,param,success,fail){
    Taro.request({
      url: baseUrl + link,
      method: 'POST',
      data: param,
      header: {
        'version': '135',
        'client': 4,
        'channel': 'applet'
      }
    }).then(async (res) => {
      await success(res.data)
    }).catch((err) => {
      wx.showToast({
        title: '服务请求出错',
        icon: 'none',
        duration: 3000
      });
      fail(err)
    })
  }

  //需要token的请求接口
  static execute(link, param, success, fail,openID) {
    var token = JSON.parse(wx.getStorageSync("USERINFO")).token;
    var headerParam = {}
    if (openID!=null&&openID!=''){
      headerParam = {
        'version': '135',
        'client': 4,
        'channel': 'applet',
        'token': token
      }
    }else
      headerParam = {
        'version': '135',
        'client': 4,
        'channel': 'applet',
        'token': token,
        'deviceId':openID
      }
    Taro.request({
      url: baseUrl + link,
      method: 'POST',
      data: param,
      header: headerParam
    }).then(async (res) => {
      await success(res.data)
    }).catch((err) => {
      wx.showToast({
        title: '服务请求出错',
        icon: 'none',
        duration: 3000
      });
      fail(err)
    })
  }

  //判断手机号码输入是否正确
  static isMPNumber(Tel){
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(19[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
    if (myreg.test(Tel)) {
      return true;
    } else {
      return false;
    }
  }

  //获取星期几
  static getWeekday(index){
    return week[index];
  }

  //导入token
  static setToken(t){
    this.token = t;
  }

  //计算时间差
  static timeFn(d1) {//di作为一个变量传进来 //如果时间格式是正确的，那下面这一步转化时间格式就可以不用了 
    var dateBegin = new Date(d1.replace(/-/g, "/"));
    //将-转化为/，使用new Date 
    var dateEnd = new Date();
    //获取当前时间 
    var dateDiff = dateEnd.getTime() - dateBegin.getTime();
    //时间差的毫秒数 
    var dayDiff = Math.floor(dateDiff / (24 * 3600 * 1000));
    //计算出相差天数 
    var leave1=dateDiff%(24*3600*1000) 
    //计算天数后剩余的毫秒数 
    var hours=Math.floor(leave1/(3600*1000))
    //计算出小时数 //计算相差分钟数 
    var leave2=leave1%(3600*1000) 
    //计算小时数后剩余的毫秒数 
    var minutes=Math.floor(leave2/(60*1000))
    //计算相差分钟数 //计算相差秒数 
    var leave3=leave2%(60*1000) 
    //计算分钟数后剩余的毫秒数 
    var seconds=Math.round(leave3/1000);
    return dayDiff;
  }
}